# Pokemon Trainer

Application created with Angular.js
Database is hosted local with json-server. The json file is pokemonDB.json
Bootstrap used for styling.

# Login

Login is based on localstorage. If the key 'pkmtr' has a value you will be forwarded to the main page.
If the key doesnt have value only login site is available.

# Catalogue

This page displays 10 Pokèmons from the API. Images from Pokeapi.co
Select the pokemon you want to collect and press the "collect" button to add it to your collection.
Go to Trainer site to view your collected Pokemon

# Trainer

View your collected pokemon

Clicking logout will remove the localstorage key "pkmtr" and return you to the login site.
