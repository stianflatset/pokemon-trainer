import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login/login.component';
import { TrainerPageComponent } from './trainer/trainer-page/trainer-page.component';
import { CataloguePageComponent } from './catalogue/catalogue-page/catalogue-page.component';
import { FormsModule } from '@angular/forms';
import { PageNotFoundComponent } from './PageNotFound/page-not-found/page-not-found.component';
import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar/navbar.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TrainerPageComponent,
    CataloguePageComponent,
    PageNotFoundComponent,
    NavbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
