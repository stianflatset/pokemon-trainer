export interface Username {
    id: string;
    pokemon: string;
    url: string
}