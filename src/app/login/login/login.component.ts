import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(private router: Router, private readonly userService: UsersService) {
    //If the user has a "pkmtr" (pokemon trainer) key redirect to catalogue site 
    if(localStorage.getItem('pkmtr') !== null) {
      this.router.navigate(['/catalogue'])      
    }

  }

  //Adding to localstorage and redirect to trainer page
  public onSubmit(createForm: NgForm): void {
    localStorage.setItem('pkmtr', createForm.value.username);
    this.userService.addUser(createForm.value.username)
    this.router.navigate(['/catalogue'])
    
  }
}
